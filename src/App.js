import './App.css';
import { Route,  BrowserRouter, Routes, Link} from 'react-router-dom';
import Home from './Home';
import LoginPage from './Pages/LoginPage';
import NotFoundPage from './Pages/NotFoundPage';
import RegisterPage from './Pages/RegisterPage';

function App() {
  return (
    <div className='App'>
      <BrowserRouter>
  
        <Routes>
          <Route index element={<Home />} />
          <Route path="404" element={<NotFoundPage />} />
          <Route path="register" element={<RegisterPage />} />
          <Route path="login" element={<LoginPage />} />
        </Routes>
          <Link to={"/"}>Home</Link>
      </BrowserRouter>
    </div>
  );
}

export default App;